import { Controller, HttpCode, Param, Get, Post, Body, Delete, Put, HttpStatus } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { DecodedMsgsService } from '../services/decodedMsg.service';
import { GetDecodedMsgResponse, GetDecodedMsgsResponse } from '../models/responses/getDecodedMsg.responses';
import { FindOneOptions } from 'typeorm';
import { DecodedMsg } from '../entities/decodedMsg.entity';
import { DecodedMsgDto } from '../models/dto/decodedMsg.dto';
import { GenericResponse } from '../models/responses/generic-response.responses';

@Controller('decoded-msg')
@ApiUseTags('decoded-msg')
export class DecodedMsgsController {
    constructor(
        private decodedMsgsService: DecodedMsgsService,
    ) {
        //
    }

    @Get(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get decodedMsg', operationId: 'getDecodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get decodedMsg', type: GetDecodedMsgResponse })
    @HttpCode(HttpStatus.OK)
    async get(@Param('id') id: string): Promise<GetDecodedMsgResponse> {
        const findOneOptions: FindOneOptions<DecodedMsg> = { where: { id } };
        return await this.decodedMsgsService.findOne(findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all decoded messages', operationId: 'getAllDecodedMsgs' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get all decoded messages', type: GetDecodedMsgsResponse })
    @HttpCode(HttpStatus.OK)
    async getAll(): Promise<GetDecodedMsgsResponse> {
        return await this.decodedMsgsService.findAll({});
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create decoded msg', operationId: 'CreateDecodedMsg' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Create decoded msg', type: GetDecodedMsgResponse })
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() decodedMsg: DecodedMsgDto): Promise<GetDecodedMsgResponse> {
        return await this.decodedMsgsService.create(decodedMsg);
    }

    @Put()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update decoded msg', operationId: 'UpdateDecodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Update decoded msg', type: GetDecodedMsgResponse })
    @HttpCode(HttpStatus.OK)
    async update(@Body() decodedMsg: DecodedMsgDto): Promise<GetDecodedMsgResponse> {
        return await this.decodedMsgsService.update(decodedMsg);
    }

    @Delete(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete decoded msg', operationId: 'DeleteDecodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Delete decoded msg', type: GenericResponse })
    @HttpCode(HttpStatus.OK)
    async delete(@Param('id') id: string): Promise<GenericResponse> {
        return await this.decodedMsgsService.delete(id);
    }
}
