import { Controller, Get, HttpCode, Headers, HttpStatus, Param, Post, Put, Delete, Body } from '@nestjs/common';
import { ApiUseTags, ApiOperation, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';
import { GetCodedMsgResponse, GetCodedMsgsResponse } from 'src/models/responses/getCodedMsg.responses';
import { CodedMsgService } from '../services/codedMsg.service';
import { FindOneOptions } from 'typeorm';
import { CodedMsg } from '../entities/codedMsg.entity';
import { CodedMsgDto } from '../models/dto/codedMsg.dto';
import { GenericResponse } from '../models/responses/generic-response.responses';

@Controller('coded-msg')
@ApiUseTags('coded-msg')
export class CodedMsgController {
    constructor(
        private readonly codedMsgService: CodedMsgService,
    ) { }

    @Get(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get coded msg', operationId: 'getCodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get coded msg', type: GetCodedMsgResponse })
    @HttpCode(HttpStatus.OK)
    async get(@Param('id') id: string, @Headers() header): Promise<GetCodedMsgResponse> {
        const findOneOptions: FindOneOptions<CodedMsg> = { where: { id } };
        return await this.codedMsgService.findOne(header.accept, header.authorization, findOneOptions);
    }

    @Get()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Get all coded messages', operationId: 'getAllCodedMsgs' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Get all coded messages', type: GetCodedMsgsResponse })
    @HttpCode(HttpStatus.OK)
    async getAll(): Promise<GetCodedMsgsResponse> {
        return await this.codedMsgService.findAll({});
    }

    @Post()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Create coded msg', operationId: 'CreateCodedMsg' })
    @ApiResponse({ status: HttpStatus.CREATED, description: 'Create coded msg', type: GetCodedMsgResponse })
    @HttpCode(HttpStatus.CREATED)
    async create(@Body() codedMsg: CodedMsgDto): Promise<GetCodedMsgResponse> {
        return await this.codedMsgService.create(codedMsg);
    }

    @Put()
    @ApiBearerAuth()
    @ApiOperation({ title: 'Update coded msg', operationId: 'UpdateCodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Update coded msg', type: GetCodedMsgResponse })
    @HttpCode(HttpStatus.OK)
    async update(@Body() decodedMsg: CodedMsgDto): Promise<GetCodedMsgResponse> {
        return await this.codedMsgService.update(decodedMsg);
    }

    @Delete(':id')
    @ApiBearerAuth()
    @ApiOperation({ title: 'Delete coded msg', operationId: 'DeleteCodedMsg' })
    @ApiResponse({ status: HttpStatus.OK, description: 'Delete coded msg', type: GenericResponse })
    @HttpCode(HttpStatus.OK)
    async delete(@Param('id') id: string): Promise<GenericResponse> {
        return await this.codedMsgService.delete(id);
    }
}
