import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { DecodedMsgDto } from '../models/dto/decodedMsg.dto';

@Entity({ name: 'decoded_msg' })
export class DecodedMsg {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('datetime', { name: 'date', nullable: false })
    date: Date;

    @Column('varchar', { name: 'userName', nullable: false })
    userName: string;

    @Column('varchar', { name: 'codedMsg', nullable: false })
    codedMsg: string;

    @Column('varchar', { name: 'decodedMsg', nullable: false })
    decodedMsg: string;

    public toDto(): DecodedMsgDto {
        return {
            id: this.id,
            date: this.date,
            userName: this.userName,
            codedMsg: this.codedMsg,
            decodedMsg: this.decodedMsg,
        };
    }

    public fromDto(dto: DecodedMsgDto) {
        this.id = dto.id;
        this.date = dto.date;
        this.userName = dto.userName;
        this.codedMsg = dto.codedMsg;
        this.decodedMsg = dto.decodedMsg;

        if (!this.id) {
            this.id = undefined;
        }
    }
}
