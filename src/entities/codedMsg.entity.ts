import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { CodedMsgDto } from '../models/dto/codedMsg.dto';

@Entity({ name: 'coded_msg' })
export class CodedMsg {
    @PrimaryGeneratedColumn('uuid', { name: 'id' })
    id: string;

    @Column('varchar', { name: 'label', nullable: false })
    label: string;

    @Column('text', { name: 'algo', nullable: false })
    algo: string;

    public toDto(): CodedMsgDto {
        return {
            id: this.id,
            label: this.label,
            algo: this.algo,
        };
    }

    public fromDto(dto: CodedMsgDto) {
        this.id = dto.id;
        this.label = dto.label;
        this.algo = dto.algo;

        if (!this.id) {
            this.id = undefined;
        }
    }
}
