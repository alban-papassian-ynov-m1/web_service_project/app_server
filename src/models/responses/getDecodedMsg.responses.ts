import { ApiModelProperty } from '@nestjs/swagger';
import { GenericResponse } from './generic-response.responses';
import { DecodedMsgDto } from '../dto/decodedMsg.dto';

export class GetDecodedMsgResponse extends GenericResponse {
    @ApiModelProperty()
    decodedMsgs: DecodedMsgDto;

    constructor() {
        super();
        this.decodedMsgs = null;
    }
}

// tslint:disable-next-line: max-classes-per-file
export class GetDecodedMsgsResponse extends GenericResponse {
    @ApiModelProperty()
    decodedMsgs: DecodedMsgDto[];

    @ApiModelProperty()
    totalDecodedMsgsCount: number;

    constructor() {
        super();
        this.decodedMsgs = [];
        this.totalDecodedMsgsCount = 0;
    }
}
