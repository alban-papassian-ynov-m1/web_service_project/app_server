import { ApiModelProperty } from '@nestjs/swagger';
import { GenericResponse } from './generic-response.responses';
import { CodedMsgDto } from '../dto/codedMsg.dto';

export class GetCodedMsgResponse extends GenericResponse {

    @ApiModelProperty({ type: () => CodedMsgDto })
    codedMsg: CodedMsgDto;

    constructor() {
        super();
        this.codedMsg = null;
    }
}

export class GetCodedMsgsResponse extends GenericResponse {

    @ApiModelProperty({ type: () => CodedMsgDto, isArray: true })
    codedMsgs: CodedMsgDto[];

    @ApiModelProperty()
    totalCountCodedMsgs: number;

    constructor() {
        super();
        this.codedMsgs = null;
        this.totalCountCodedMsgs = null;
    }
}
