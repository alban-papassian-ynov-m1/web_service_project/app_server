import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class DecodedMsgDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    date: Date;

    @ApiModelProperty()
    userName: string;

    @ApiModelProperty()
    codedMsg: string;

    @ApiModelProperty()
    decodedMsg: string;
}
