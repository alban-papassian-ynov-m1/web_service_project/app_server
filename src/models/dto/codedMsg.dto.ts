import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class CodedMsgDto {
    @ApiModelPropertyOptional()
    id?: string;

    @ApiModelProperty()
    label: string;

    @ApiModelProperty()
    algo?: string;
}
