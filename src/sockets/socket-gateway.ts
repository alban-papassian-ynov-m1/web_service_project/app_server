
import { WebSocketGateway, SubscribeMessage, WebSocketServer, OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit } from '@nestjs/websockets';
import { Client, Server, Socket } from 'socket.io';
import { DecodedMsgsService } from '../services/decodedMsg.service';
import { CodedMsgService } from '../services/codedMsg.service';
import { HttpService } from '@nestjs/common';
import { GetUserResponse } from '../services/codedMsg.service';

export interface SocketEventPayload {
    date?: Date;
    data: any;
}

export interface UserConnections {
    userId: string;
    connections: string[];
}

export interface WebSocketPayload {
    date?: Date;
    methode: string;
    token: string;
    data: {
        success: boolean;
        username?: string;
        msgCoded?: string;
        msgDecoded?: string;
    };
}

@WebSocketGateway()
export class SocketGateway implements OnGatewayConnection, OnGatewayDisconnect, OnGatewayInit {

    constructor(
        private decodedMsgsService: DecodedMsgsService,
        private codedMsgService: CodedMsgService,
        private http: HttpService,
    ) {
        console.log('SocketGateway constructor');
    }

    @WebSocketServer()
    server: Server;

    public static UserConnectionsList: UserConnections[] = [];
    public static AllConnections: string[] = [];

    handleDisconnect(client: any) {
        let indexToRemove = SocketGateway.AllConnections.indexOf(client.id);
        if (indexToRemove !== -1)
            SocketGateway.AllConnections.splice(indexToRemove, 1);
        const userConnections = SocketGateway.UserConnectionsList.find(x => x.connections.some(y => y === client.id));
        if (userConnections) {
            indexToRemove = userConnections.connections.indexOf(client.id);
            if (indexToRemove !== -1)
                userConnections.connections.splice(indexToRemove, 1);
        }
    }

    handleConnection(client: Socket, ...args: any[]) {
        SocketGateway.AllConnections.push(client.id);
    }

    afterInit(server: Server) {
        console.log('SocketGateway afterInit');
    }

    @SubscribeMessage('events')
    handleEvent(client: Client, data: string): string {
        return data;
    }

    @SubscribeMessage('viveLaFrance')
    async viveLaFrance(client: Client, payload: WebSocketPayload) {
        const getCodedMsg = await this.codedMsgService.findAll({ where: { label: payload.data.msgCoded } });
        if (getCodedMsg.success && getCodedMsg.codedMsgs.length > 0) {
            const deleteCodedMsg = await this.codedMsgService.delete(getCodedMsg.codedMsgs[0].id);
        }
        const save = await this.decodedMsgsService.create({
            codedMsg: payload.data.msgCoded,
            date: new Date(),
            decodedMsg: payload.data.msgDecoded,
            userName: payload.data.username,
        });

    }

    @SubscribeMessage('laFranceAPerduUneBataille-maisPasLaGuerre')
    laFranceAPerduUneBatailleMaisPasLaGuerre() {
        console.log('pas ok');
    }

    // Expliquer pourquoi : structure / fonction
    public sendEventToClient(evt: string, data?: SocketEventPayload, userIds?: string[]) {
        if (data) {
            if (!data.date)
                data.date = new Date();
        }
        if (!userIds)
            this.server.clients().emit(evt, data);
        else {
            if (!SocketGateway.UserConnectionsList)
                return;
            const connections = SocketGateway.UserConnectionsList.filter(x => userIds.indexOf(x.userId) !== -1);
            const connectionsIds: string[] = [];
            connections.forEach(conn => {
                connectionsIds.push(...conn.connections);
            });
            if (connectionsIds.length > 0) {
                connectionsIds.forEach(connectionId => {
                    this.server.to(connectionId).emit(evt, data);
                });

            }
        }
    }
}
