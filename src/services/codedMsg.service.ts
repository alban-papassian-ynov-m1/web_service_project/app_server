import { Injectable, HttpService } from '@nestjs/common';
import { GetCodedMsgResponse, GetCodedMsgsResponse } from 'src/models/responses/getCodedMsg.responses';
import { GenericResponse } from '../models/responses/generic-response.responses';
import { InjectRepository } from '@nestjs/typeorm';
import { CodedMsg } from '../entities/codedMsg.entity';
import { Repository, FindManyOptions, FindOneOptions } from 'typeorm';
import { CodedMsgDto } from '../models/dto/codedMsg.dto';

export interface UserDto {
    id?: string;
    username: string;
    password: string;
}

export interface GetUserResponse {
    success: boolean;
    message?: string;
    error?: any;
    token?: string;
    statusCode?: number;
    user: UserDto;
}

@Injectable()
export class CodedMsgService {
    constructor(
        private http: HttpService,
        @InjectRepository(CodedMsg)
        private readonly codedMsgRepository: Repository<CodedMsg>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<CodedMsg>): Promise<GetCodedMsgsResponse> {
        const response = new GetCodedMsgsResponse();
        try {
            conditions.relations = [];
            const getCodedMsgs = await this.codedMsgRepository.find(conditions);

            if (getCodedMsgs) {
                response.codedMsgs = getCodedMsgs.map(x => x.toDto());
                response.totalCountCodedMsgs = await this.codedMsgRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(language: string, token: string, conditions?: FindOneOptions<CodedMsg>): Promise<GetCodedMsgResponse> {
        const response = new GetCodedMsgResponse();
        try {
            const tokenResponse = await this.http.get<GetUserResponse>('http://localhost:3001' + '/auth/check-token/' + token).toPromise();
            if (tokenResponse.data.success) {
                if (language === 'application/javascript') {
                    conditions.relations = [];
                    const getcodedMsg = await this.codedMsgRepository.findOne(conditions);

                    if (getcodedMsg) {
                        response.codedMsg = getcodedMsg.toDto();
                    }

                    response.message = 'OK - //';
                } else {
                    response.message = 'Je ne connais pas ce langage, surement un langage ennemi';
                }

                response.success = true;
            } else {
                response.success = false;
                response.message = 'Token incorect';
            }
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async create(codedMsg: CodedMsgDto): Promise<GetCodedMsgResponse> {
        const response = new GetCodedMsgResponse();
        try {
            let codedMsgEntity = new CodedMsg();

            if (!codedMsg.algo) {
                codedMsg.algo = 'function cesar(str, amount) {'
                    + 'if (amount < 0)'
                    + 'return cesar(str, amount + 26);'
                    + 'var res = \'\';'
                    + 'for (var i = 0; i < str.length; i++) {'
                    + ' var c = str[i];'
                    + 'if (c.match(/[a-z]/i)) {'
                    + 'var code = str.charCodeAt(i);'
                    + 'if ((code >= 65) && (code <= 90))'
                    + 'c = String.fromCharCode(((code - 65 - amount) % 26) + 65);'
                    + 'else if ((code >= 97) && (code <= 122))'
                    + 'c = String.fromCharCode(((code - 97 - amount) % 26) + 97);'
                    + '}'
                    + 'res += c;'
                    + '}'
                    + 'return res;'
                    + '};'
                    + 'let find = false;'
                    + 'let cpt = 0;'
                    + 'do {'
                    + 'let txtDecoded = cesar(\'' + codedMsg.label + '\', cpt);'
                    + 'if (txtDecoded.search(\'DEGAULLE\') !== -1 || txtDecoded.search(\'DeGaulle\')!== -1 || txtDecoded.search(\'degaulle\')!== -1) {'
                    + 'this.socketService.sendEvent(\'viveLaFrance\', { methode: \'POST\', token: this.token, data: { success: true, msgCoded: \'' + codedMsg.label + '\', msgDecoded: txtDecoded, username: this.username } });'
                    + 'find = true;'
                    + '} else {'
                    + 'cpt++;'
                    + '}'
                    + '} while (!find && cpt < 27);'
                    + 'if (!find) {'
                    + 'this.socketService.sendEvent(\'laFranceAPerduUneBataille-maisPasLaGuerre\');'
                    + '}';

                const buff = new Buffer(codedMsg.algo);
                const base64data = buff.toString('base64');
                codedMsg.algo = base64data;
            }

            codedMsgEntity.fromDto(codedMsg);

            codedMsgEntity = await this.codedMsgRepository.save(codedMsgEntity);
            codedMsgEntity = await this.codedMsgRepository.findOne({ id: codedMsgEntity.id });

            response.codedMsg = codedMsgEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async update(codedMsg: CodedMsgDto): Promise<GetCodedMsgResponse> {
        const response = new GetCodedMsgResponse();
        try {
            let codedMsgEntity = await this.codedMsgRepository.findOne({ id: codedMsg.id }, { relations: [] });
            if (!codedMsgEntity) {
                throw new Error('No coded msg with id => ' + codedMsg.id);
            }

            codedMsgEntity.fromDto(codedMsg);

            codedMsgEntity = await this.codedMsgRepository.save(codedMsg);
            codedMsgEntity = await this.codedMsgRepository.findOne({ id: codedMsgEntity.id });

            response.codedMsg = codedMsgEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(id: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const codedMsg = await this.codedMsgRepository.findOne({ id }, { relations: [] });
            if (!codedMsg) {
                throw new Error('No coded msg with id => ' + id);
            }
            await this.codedMsgRepository.delete(id);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
