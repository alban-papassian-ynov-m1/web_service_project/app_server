import { Injectable } from '@nestjs/common';
import { CodedMsgService } from './codedMsg.service';
import { CodedMsgDto } from '../models/dto/codedMsg.dto';

@Injectable()
export class DatabaseService {
    constructor(
        private codedMsgService: CodedMsgService,
    ) {
        //
    }

    async init() {
        await this.createCodedMsg();
    }

    async createCodedMsg() {
        try {
            const codedMsg: CodedMsgDto = { label: 'LOHVWWURSEHDXGHJDXOOHTXHOOHFODVVH' };

            const save = await this.codedMsgService.create(codedMsg);
        } catch (err) {
            console.error(err);
        }
    }
}
