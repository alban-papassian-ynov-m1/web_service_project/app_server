import { Injectable } from '@nestjs/common';
import { FindManyOptions, FindOneOptions, Repository } from 'typeorm';
import { GenericResponse } from '../models/responses/generic-response.responses';
import { InjectRepository } from '@nestjs/typeorm';
import { DecodedMsgDto } from '../models/dto/decodedMsg.dto';
import { DecodedMsg } from '../entities/decodedMsg.entity';
import { GetDecodedMsgResponse, GetDecodedMsgsResponse } from '../models/responses/getDecodedMsg.responses';

@Injectable()
export class DecodedMsgsService {
    constructor(
        @InjectRepository(DecodedMsg)
        private readonly decodedMsgRepository: Repository<DecodedMsg>,
    ) {
        //
    }

    async findAll(conditions?: FindManyOptions<DecodedMsg>): Promise<GetDecodedMsgsResponse> {
        const response = new GetDecodedMsgsResponse();
        try {
            conditions.relations = [];
            const getDecodedMsgs = await this.decodedMsgRepository.find(conditions);

            if (getDecodedMsgs) {
                response.decodedMsgs = getDecodedMsgs.map(x => x.toDto());
                response.totalDecodedMsgsCount = await this.decodedMsgRepository.count(conditions);
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async findOne(conditions?: FindOneOptions<DecodedMsg>): Promise<GetDecodedMsgResponse> {
        const response = new GetDecodedMsgResponse();
        try {
            conditions.relations = [];
            const getDecodedMsg = await this.decodedMsgRepository.findOne(conditions);

            if (getDecodedMsg) {
                response.decodedMsgs = getDecodedMsg.toDto();
            }

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async create(decodedMsg: DecodedMsgDto): Promise<GetDecodedMsgResponse> {
        const response = new GetDecodedMsgResponse();
        try {
            let decodedMsgEntity = new DecodedMsg();

            decodedMsgEntity.fromDto(decodedMsg);

            decodedMsgEntity = await this.decodedMsgRepository.save(decodedMsg);
            decodedMsgEntity = await this.decodedMsgRepository.findOne({ id: decodedMsgEntity.id });

            response.decodedMsgs = decodedMsgEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async update(decodedMsg: DecodedMsgDto): Promise<GetDecodedMsgResponse> {
        const response = new GetDecodedMsgResponse();
        try {
            let decodedMsgEntity = await this.decodedMsgRepository.findOne({ id: decodedMsg.id }, { relations: [] });
            if (!decodedMsgEntity) {
                throw new Error('No agenda with id => ' + decodedMsg.id);
            }

            decodedMsgEntity.fromDto(decodedMsg);

            decodedMsgEntity = await this.decodedMsgRepository.save(decodedMsg);
            decodedMsgEntity = await this.decodedMsgRepository.findOne({ id: decodedMsgEntity.id });

            response.decodedMsgs = decodedMsgEntity.toDto();

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }

    async delete(decodedMsgId: string): Promise<GenericResponse> {
        const response = new GenericResponse();
        try {
            const decodedMsg = await this.decodedMsgRepository.findOne({ id: decodedMsgId }, { relations: [] });
            if (!decodedMsg) {
                throw new Error('No agenda with id => ' + decodedMsgId);
            }
            await this.decodedMsgRepository.delete(decodedMsgId);

            response.success = true;
            response.message = 'OK - //';
        } catch (err) {
            response.handleError(err);
        }

        return response;
    }
}
