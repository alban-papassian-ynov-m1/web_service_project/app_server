import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SocketGateway } from './sockets/socket-gateway';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DecodedMsg } from './entities/decodedMsg.entity';
import { DecodedMsgsService } from './services/decodedMsg.service';
import { DecodedMsgsController } from './controllers/decodedMsg.controller';
import { CodedMsgService } from './services/codedMsg.service';
import { CodedMsgController } from './controllers/codedMsg.controller';
import { DatabaseService } from './services/database.service';
import { CodedMsg } from './entities/codedMsg.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'appserver',
      entities: [
        __dirname + '/entities/*.entity{.ts,.js}',
      ],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([
      DecodedMsg,
      CodedMsg,
    ]),
    HttpModule,
  ],
  controllers: [
    AppController,
    CodedMsgController,
    DecodedMsgsController,
  ],
  providers: [
    AppService,
    CodedMsgService,
    DecodedMsgsService,
    SocketGateway,
    DatabaseService,
  ],
})
export class AppModule {
  constructor(
    private databaseService: DatabaseService,
  ) {
    this.databaseService.init();
  }
}
